INTRODUCTION
------------
This module extends the Sharpspring Module's (https://www.drupal.org/sandbox/rkimball/2356461) functionality to personalize block content based on custom Sharpspring lead fields.


 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/choicelildice/2480971


 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2480971
   
   
REQUIREMENTS
------------
This module requires the following modules:
 * Sharpspring (https://www.drupal.org/sandbox/rkimball/2356461)
 
 
INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
   
   
CONFIGURATION
-------------
 * This module requires the Sharpspring module to be configured properly, and for the user to be a qualified lead.
 
 * The configuration page can be found at "/admin/config/system/sharpspring/blocks" or by navigating to Configuration -> System -> Sharpspring -> Contextual Blocks.
 
    - You may add a new Sharpsrping Contextual Block from the configuration page. This block will act as a "container" block. Based on the "Block Rules," it will display  other system blocks.
   
    - To add a block, click "Add Block." Give the block a title and description, and save the block.
   
    - To add a rule, open the "Block Rules" tab, and click "Add Rule."
   
    - Give the rule a name and description as needed.
   
    - Choose whether the rule will fire if ALL conditions are met, or if 1 condition is met.
   
    - Add a condition (or conditions) needed for the rule to pass. These rules are simply matching a shaprspring custom field name to a field value. You can either have the field equal, or not equal, the value.
   
    - Save the rule.
   
    - Go back to the block created earlier. You should now see the rule added in the "reactions" section. You can chose which system block will be shown when that rule passes.
   
    - Save the block.

    - Place the Sharpspring contextual block using the Structure -> Blocks section, or through any other method of adding blocks to pages.
   
    - If sharpspring is configured correctly, and the rule is firing, it should replace the content of the contextual block. 